/*jslint devel: true, sloppy: true, maxerr: 50, indent: 4 */

var MYAPP = MYAPP || {};

MYAPP.namespace = (function () {

	// public functions
	return {

		homepage: function () {
			// fake input file
			$('#apply-now').find('div.apply_faUpload a').click(function(e){
				$('#apply_upload').trigger('click');
				e.preventDefault();
			});
			$('#apply_upload').change(function(){
				$('#apply-now').find('.file-value').text($(this).val());
			});
		}

	};

}());
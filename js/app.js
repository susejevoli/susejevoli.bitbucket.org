// good practice to have a closure
// [?] need to research why
(function () {

	// always start with defining an app (app = 'rcep')
	// reference ngAnimate module to use animations
	var rcep = angular.module('rcep', ['ngAnimate']);

	// service to get builds (so multiple controllers/directives can use)
	rcep.factory('getBuilds', function ($http) {
		return {
			getBuildsData: function (data) {
				$http.get('/builds/builds.json').success(data);
			}
		};
	});

	// category filter: only builds with a category key, only unique categories to output
	rcep.filter('category', function () {
		return function (builds) {
			var uniqueCatBuilds = [],
					categoryArray = [];

			angular.forEach(builds, function (build) {
				// if build is integrated
				if (build.integrated === 'true') {
					// only unique categories
					if (categoryArray.indexOf(build.category) < 0) {
						// categoryArray contains only unique categories, exists to test
						categoryArray.push(build.category);
						// uniqueCatBuilds contains first builds of new categories to output
						uniqueCatBuilds.push(build);
						// [?] why does this run 3 times? Turn on console.log to see
						//console.log('categoryArray', categoryArray);
					}
				}
			});

			return uniqueCatBuilds;
		};
	});

	// main controller, 'global scope' access to control build list
	rcep.controller('main', function ($scope) {
		// define the category filter
		// [?] why categoryFilter.category to make this work? Not just 'categoryFilter'?
		$scope.categoryFilter = {};

		// allows access to scope object in console, type 'scope'
		window.scope = $scope;
	});

	// directive to build category list + control category select behavior
	rcep.directive('buildCategories', function (getBuilds) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				var
					buildList = [],
					selectedCat = '',
					currentCat = '';

				// get builds data, make it available to the HTML
				getBuilds.getBuildsData(function (data) {
					scope.buildList = data;
				});

				// test for active category to apply filter, otherwise reset filter
				// using selected category & current category to determine toggle state
				scope.catFilterControl = function (build) {
					currentCat = build.category;
					//console.log('currentCat', currentCat, 'selectedCat', selectedCat);

					if (currentCat === selectedCat) {
						// [?] scope bubbles up to global scope??
						scope.categoryFilter.category = '';
						selectedCat = '';
					} else {
						scope.categoryFilter.category = build.category;
						selectedCat = currentCat;
					}
				};

				// jQuery love to control category select behavior
				// [?] how to do this without relying on jQuery?
				element.on('click', 'li', function () {
					var $navlist = $('#nav-list');

					$(this).toggleClass('active').siblings().removeClass();

					// if no category selected, remove filtered styling
					if (element.find('li').hasClass('active')) {
						$navlist.addClass('filtered');
					} else {
						$navlist.removeClass('filtered');
					}
				});
			}
		};
	});

	// controller to output builds wherever
	rcep.controller('buildsController', function (getBuilds) {

		// init some vars so timings don't cause error
		var builds = this;

		// [?] did I just define buildList?
		builds.buildList = [];

		// grab the build data
		getBuilds.getBuildsData(function (data) {
			// assign data from json file to our array
			builds.buildList = data;
		});

	});

})();
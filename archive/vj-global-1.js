// progress in getting search & cat-filter working together
// 	1. cat control only = pass
// 	2. search control only = pass
// 	3. search > cat = cat control fine, clear search = can't reset
// 	4. cat > search = release cat, issues encountered...
// abadoning for a better approach.

var RCEP = RCEP || {};

RCEP.framework = (function () {
	"use strict";

	var
	options = {
		viewportWidthDisplay : false,
		disableBuildLinks : true,
		animateSpeed: 200
	},
	$window = $(window),
	$body = $('body'),
	$page = $('#page-wrap'),
	$rcep = $('#rcep'),
	$buildList = $('#rcep-list'),
	$catList = $('#nav-list'),
	$filter = $('#filter'),
	filterClass = null,
	// initialize functions
	init = function() {
		viewportWidth();
		buildListLinkControl();
		events();
		getCategories();
		filterClass();
		textSearch();
	},
	// page events
	events = function() {
		// category listing buttons
		$catList.on('click', 'li', function () {
			// when category selected addClass .active to li, addClass .filtered to #nav-list
			var $this = $(this),
				category = $this.text(),
				reset = false;
			// class control for visual button behavior
			$this.toggleClass('active').siblings().removeClass();
			if ($catList.find('.active').length > 0) {
				$this.parent().addClass('filtered');
			} else {
				$this.parent().removeClass('filtered');
				reset = true;
			}
			//
			if ($filter.val() === '') {
				filterClass = 'cat-filtered';
			}
			// category filtering
			filterCategories(category, reset);
		});
	},
	// viewport width helper, switch on via options object above
	viewportWidth = function() {
		if (options.viewportWidthDisplay === false) return false;

		$body.append('<div id="vsize"></div>');

		var $vsize = $('#vsize'),
			stopResize;

		$vsize.css({
			'position': 'fixed',
			'top': 0,
			'right': 0,
			'background-color': 'rgba(255, 255, 255, 0.5)',
			'color': '#000',
			'padding': '2px 5px'
		});

		// call function once on page init to show current width
		$vsize.text('w: ' + $page.width() + 'px').delay(1000).fadeOut();

		// show width as viewport resized, fade out after resizing stopped
		$window.on('resize', function() {
			$vsize.show().text('w: ' + $page.width() + 'px');
			clearTimeout(stopResize);
			stopResize = setTimeout(function() {
				$vsize.fadeOut();
			}, 1000);
		});
	},
	// disable build list links
	buildListLinkControl = function() {
		if (options.disableBuildLinks === false) return false;
		$buildList.find('a').on('click', function() {
			return false;
		});
	},
	// get & display all unique categories from build list
	getCategories = function() {

		var catArray = [];

		// get all categories, if category not in array already, then add to array
		$buildList.find('.category').each(function() {
			var $this = $(this);
			if ($.inArray($this.text(), catArray) === -1) {
				catArray.push($this.text());
			}
		});

		// print each item in array to category nav
		$.each(catArray, function(index, item) {
			$catList.append('<li>' + item + '</li>');
		});
	},
	//
	filterClass = function() {
		$filter.on({
			focus: function() {
				if ($filter.val() === '' && $catList.find('.active').length === 0) {
					// if searching first (no category selected) = search class
					filterClass = 'search-filtered';
					console.log(filterClass);
				} else {
					// if category selected first, then search = cat class
					filterClass = 'cat-filtered';
					console.log(filterClass);
				}
			},
			blur: function() {
				if ($filter.val() === '') {
					filterClass = 'cat-filtered';
					console.log(filterClass);
				}
			}
		});
	},
	// display selected category
	filterCategories = function(category, reset) {
		var catClass = null,
			target = null;
		//
		target = 'li';
		//
		if (filterClass === 'cat-filtered') {
			catClass = filterClass;
		} else if (filterClass === 'search-filtered') {
			catClass = '';
			target = '.' + filterClass;
		} else {
			catClass = '';
		}
		console.log(target);
		if (reset === false) {
			$buildList.children(target).each(function() {
				var $this = $(this);
				if ($this.find('.category:contains(' + category + ')').length) {
					$this.slideDown(options.animateSpeed).addClass(catClass);
				} else {
					console.log('1');
					$this.slideUp(options.animateSpeed).removeClass(catClass);
				}
			});
		} else {
			console.log('2');
			if ($filter.val() != '') {
				target = '.' + filterClass;
			}
			$buildList.children(target).slideDown(options.animateSpeed).removeClass(catClass);
		}
	},
	// build keyword search
	textSearch = function() {
		$filter.on('keyup', function() {
			// retrieve text from search box
			var $this = $(this),
				filter = $this.val(),
				target = null,
				searchClass;
			// if category is selected, only search within that category
			target = 'li';
			// if searching first, add search class so category select only filters cat of search results
			if (filterClass === 'search-filtered') {
				searchClass = filterClass;
			} else if (filterClass === 'cat-filtered') {
				searchClass = '';
				target = '.' + filterClass;
			} else {
				searchClass = '';
			}
			// loop through build list
			$buildList.children(target).each(function() {
				var $build = $(this);
				if ($build.text().search(new RegExp(filter, "i")) < 0) {
					// hide builds that don't contain keyword
					$build.slideUp(options.animateSpeed).removeClass(searchClass);
				} else {
					// show builds that contain keyword
					$build.slideDown(options.animateSpeed).addClass(searchClass);
				}
			});
			// if clear search field, remove all search classes
			if ($this.val() === '' && filterClass != 'cat-filtered') {
				console.log('reset');
				$buildList.children().removeClass();
			}
			console.log(filterClass);
		});
	};

	// public functions
	return {
		init : init
	};

}());
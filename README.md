# re:metronome

Before I started working on the Holden account full-time, I was a studio resource working across multiple clients such as: Australian Childhood Foundation, Porter Davis, Quest Apartments, Dulux, Sensis, Jetstar and more.

The Reusable Components & Experimental Projects list was my attempt at the time (2013-14) to build a personal library of front-end code that was easily accessible for future reference.

Everything listed is super old so I'm not sure if it's of much relevance, but feel free to [have a look](http://susejevoli.bitbucket.org/). The ACF builds require server-side includes to work. The main ACF website has had a refresh since the original build, but the [Kids Count website](http://www.kidscount.com.au/en/) is largely the same.

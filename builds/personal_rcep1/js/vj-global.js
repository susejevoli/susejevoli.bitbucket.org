
var RCEP = RCEP || {};

RCEP.framework = (function () {
	"use strict";

	var
	options = {
		viewportWidthDisplay : false,
		disableBuildLinks : true,
		animateSpeed: 200
	},
	$window = $(window),
	$body = $('body'),
	$page = $('#page-wrap'),
	$rcep = $('#rcep'),
	$buildList = $('#rcep-list'),
	$catList = $('#nav-list'),
	$filter = $('#filter'),
	searchPhrase = '',
	catSelected = '',
	// initialize functions
	init = function() {
		viewportWidth();
		buildListLinkControl();
		events();
		getCategories();
	},
	// page events
	events = function() {
		// category listing buttons
		$catList.on('click', 'li', function () {
			// when category selected addClass .active to li, addClass .filtered to #nav-list
			var $this = $(this);
			// class control for visual button behavior
			$this.toggleClass('active').siblings().removeClass();

			if ($catList.find('.active').length > 0) {
				$this.parent().addClass('filtered');
				catSelected = $this.text();
			} else {
				$this.parent().removeClass('filtered');
				catSelected = '';
			}
			// filter build list
			buildFilterRequirements();
		});
		// search field bindings
		$filter.on({
			keypress: function(e) {
				if (event.keyCode == 13) {
					// if pressed enter key
					searchPhrase = $filter.val();
					buildFilterRequirements();
					e.preventDefault();
				}
			},
			keyup: function() {
				if ($filter.val() === '') {
					// if clearing search, reset build list - show all builds again
					searchPhrase = $filter.val();
					buildFilterRequirements();
				}
			}
		});
	},
	// viewport width helper, switch on via options object above
	viewportWidth = function() {
		if (options.viewportWidthDisplay === false) return false;

		$body.append('<div id="vsize"></div>');

		var $vsize = $('#vsize'),
			stopResize;

		$vsize.css({
			'position': 'fixed',
			'top': 0,
			'right': 0,
			'background-color': 'rgba(255, 255, 255, 0.5)',
			'color': '#000',
			'padding': '2px 5px'
		});

		// call function once on page init to show current width
		$vsize.text('w: ' + $page.width() + 'px').delay(1000).fadeOut();

		// show width as viewport resized, fade out after resizing stopped
		$window.on('resize', function() {
			$vsize.show().text('w: ' + $page.width() + 'px');
			clearTimeout(stopResize);
			stopResize = setTimeout(function() {
				$vsize.fadeOut();
			}, 1000);
		});
	},
	// disable build list links
	buildListLinkControl = function() {
		if (options.disableBuildLinks === false) return false;
		$buildList.find('a').on('click', function() {
			return false;
		});
	},
	// get & display all unique categories from build list
	getCategories = function() {

		var catArray = [];

		// get all categories, if category not in array already, then add to array
		$buildList.find('.category').each(function() {
			var $this = $(this);
			if ($.inArray($this.text(), catArray) === -1) {
				catArray.push($this.text());
			}
		});

		// print each item in array to category nav
		$.each(catArray, function(index, item) {
			$catList.append('<li>' + item + '</li>');
		});
	},
	// work out filter requirements
	buildFilterRequirements = function() {
		var searchFilter = "$build.text().search(new RegExp(searchPhrase, 'i')) > 0",
			catFilter = "$build.find('.category:contains(' + catSelected + ')').length";

		if (searchPhrase != '' && catSelected != '') {
			// search keyword within category
			filterBuilds("(" + searchFilter + ") && (" + catFilter + ")");
		} else if (searchPhrase != '' && catSelected === '') {
			// just searching keywords
			filterBuilds(searchFilter);
		} else if (searchPhrase === '' && catSelected != '') {
			// just filtering builds by catSelected
			filterBuilds(catFilter);
		} else {
			// resetting build list to show all
			filterBuilds('');
		}
	},
	// filter - show/hide builds in build list
	filterBuilds = function(filterRequirements) {
		$buildList.children().each(function() {
			var $build = $(this);
			if (filterRequirements != '') {
				if (eval(filterRequirements)) {
					// show builds
					$build.slideDown(options.animateSpeed);
				} else {
					// hide builds
					$build.slideUp(options.animateSpeed);
				}
			} else {
				$build.slideDown(options.animateSpeed);
			}
		});
	};

	// public functions
	return {
		init : init
	};

}());
/*jslint devel: true, sloppy: true, maxerr: 50, indent: 4 */

// why === here; selectedAnswers == answersRequired, does not work?

var PDAVIS = PDAVIS || {};

PDAVIS.wosquiz = (function(doc, $, undefined) {

	"use strict";

	// Global Vars
	var $wosQuiz = $('#worldOfStyleQuiz'),
		$currentSlide = null,
		$currentGrid = null,
		$imgClicked = null,
		$proceedBtn = null,
		$nextSlide = null,
		$resultsPage = null,
		answersRequired = null,
		selectedAnswers = null,
		progress = null,
		category = null,
		voteCategory = null,
		group = null,
		voteGroup = null,
		mostVotes = null,
		part1 = null,
		part2 = null,
		totalSlides = null,
		currentSlide = null,
		progressPercent = null,
		categoryId = null,
		style = null,
		list = null,
		classic = 0,
		contemporary = 0,
		designer = 0,
		resort = 0,
		group1 = 0,
		group2 = 0,
		group3 = 0,
		group4 = 0,
		website = 'http://www.porterdavis.com.au',
		queryString = window.location.search,
		Services = {
			styleresults: "/api/InteriorStyles/"
		},
		init = function() {
			// Initiate pd.wos-quiz.js if World of Style Quiz found on page load
			if ($wosQuiz.length) {
				events();
				skipToResults();
			}
		},
		// Page Events
		events = function() {

			// next button, can only proceed if not disabled
			$wosQuiz.find('a.btn-proceed').on('click', function(e) {
				e.preventDefault();

				var $this = $(this);
				$currentSlide = $this.parents('.module-wrap');
				progress = parseInt($currentSlide.attr('data-progress'), 10);

				if (!$this.hasClass('disabled')) {
					// proceed, increment progress & find module with that number
					progress = progress + 1;
					// call progress control figure out next slide & what to do in those slides
					progressControl($this, progress);
				}
			});

			// quiz image/answer select
			$wosQuiz.find('a.wosq-select').on('click', function(e) {

				var $this = $(this),
					$currentGrid = $this.parents('ul');

				if (!$currentGrid.hasClass('wosq-grid-result')) {
					// if NOT results page (end of quiz)
					e.preventDefault();

					if ($this.hasClass('wosq-active')) {
						// if image/answer is selected, deselect, subtract answer from tally
						$this.removeClass('wosq-active');
						answerCounter($this, 'subtract');
					} else if (!$this.hasClass('wosq-active') && !$currentGrid.hasClass('wosq-disable')) {
						// if image/answer is not selected, select, unless grid is disabled & add answer to tally
						$this.addClass('wosq-active');
						answerCounter($this, 'add');
					}
					checkIfCanProceed($this);
				}
			});

			// restart quiz btn
			$wosQuiz.find('.wosq-restart-quiz').on('click', function() {
				// delete results cookie & go back to start of quiz
				$.removeCookie('wosq-results', { path: '/'});
				window.location.href = document.location.protocol + "//" + document.domain;
			});
		},
		// determine if user wants to skip to results, either via URL or cookie (created once quiz completed)
		skipToResults = function(){
			var $proceedBtn = null,
				cookie = null,
				URLvars = [];

			$.cookie.raw = true;

			if (queryString.length) {
				$proceedBtn = $wosQuiz.find('div.wosq-start a.btn-proceed');
				progress = $wosQuiz.find('div[data-result="style"]').attr('data-progress');
				URLvars = queryString.replace('?', '').split('&');
				URLvars = $.map(URLvars, function (val) {
					return val.split('=');
				});
				category = URLvars[1];
				categoryId = URLvars[3];
				group = URLvars[5];
				progressControl($proceedBtn, progress);
				//console.log(category, categoryId, group);
			} else if ($.cookie('wosq-results')) {
				$proceedBtn = $wosQuiz.find('div.wosq-start a.btn-proceed');
				progress = $wosQuiz.find('div[data-result="style"]').attr('data-progress');
				cookie = $.cookie('wosq-results').split(',');
				category = cookie[0];
				categoryId = cookie[1];
				group = cookie[2];
				progressControl($proceedBtn, progress);
			} else {
				// if we don't need results, start quiz
				$wosQuiz.children('div.wosq-start').show();
			}
		},
		// determine if Image/Answer count correct to proceed
		checkIfCanProceed = function($imgClicked) {
			var $currentSlide = $imgClicked.parents('div.module-wrap'),
				$currentGrid = $currentSlide.find('.wosq-grid'),
				$proceedBtn = $currentSlide.find('a.btn-primary'),
				answersRequired = $currentSlide.attr('data-answers-required'),
				selectedAnswers = $currentSlide.find('.wosq-active').length;

			// disable/enable I/A grid if no. of required answers selected & enable/disable next button
			if (selectedAnswers == answersRequired) {
				$currentGrid.addClass('wosq-disable');
				$proceedBtn.removeClass('disabled');
			} else if (selectedAnswers != answersRequired && $currentGrid.hasClass('wosq-disable')) {
				$currentGrid.removeClass('wosq-disable');
				$proceedBtn.addClass('disabled');
			}
		},
		// progress control - figure out next slide & what to do in those slides
		progressControl = function($proceedBtnClicked, progress) {
			var $nextSlide = $wosQuiz.find('div[data-progress="' + progress + '"]');

			// hide this module
			$proceedBtnClicked.parents('.module-wrap').hide();
			// update progress bar
			progressBar($nextSlide);

			if ($nextSlide.attr('data-category')) {
				// which category - part 2, progress down p2 questions by appropriate category
				$wosQuiz.find('div[data-progress="' + progress + '"][data-category="' + category + '"]').show();

			} else if ($nextSlide.attr('data-result') === 'category') {
				// result - end part 1, tally p1 answers & return appropriate category on this page, update category var, get category ID
				tallyAnswers('part1');
				$resultsPage = $wosQuiz.find('div[data-progress="' + progress + '"][data-result="category"]');
				categoryId = $resultsPage.attr('data-' + category + '-id');
				$resultsPage.find('.wosq-cat-output').text(category);
				$resultsPage.find('.wosq-cat-img').attr('src', 'img/content/img-category-' + category + '.jpg');
				$resultsPage.show();
			} else if ($nextSlide.attr('data-result') === 'style') {
				// result - end part 2/quiz, tally p2 answers, do API call to return appropriate style group, build HTML from data returned
				// update social media links with proper to-share content, add results loader class
				// remove & inject google map to avoid marker-off-center issue caused by hidden DIV
				$resultsPage = $wosQuiz.find('div[data-progress="' + progress + '"][data-result="style"]');
				if (queryString === '' && !$.cookie('wosq-results')) {
					tallyAnswers('part2');
				}
				$resultsPage.find('iframe.wosq-map').remove();
				$resultsPage.find('.wosq-show').removeClass('wosq-show');
				$resultsPage.find('.wosq-cat-output').text(category);
				$resultsPage.find('span[data-result-cat="' + category + '"][data-result-group="' + group + '"]').addClass('wosq-show');
				$resultsPage.find('ul.wosq-grid-result').addClass('wosq-loader').empty();
				if ($('html').hasClass('xdomainrequest')) {
					// IE9/8/7
					console.log(category, ' ', categoryId, ' ', group);
					xDomainRequest($resultsPage);
				} else {
					// every other browser
					$.getJSON(getStyleGroup(categoryId, group), function(data) {
						buildStyleList(data, $resultsPage);
					});
				}
				updateSocialMediaLinks();
				$.cookie('wosq-results', category + ',' + categoryId + ',' + group, { expires: 1, path: '/' });
				$resultsPage.show().find('.wosq-location').prepend('<iframe class="wosq-map" width="382" height="228" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.au/maps?q=Level+1,+32+Fennell+Street+Port+Melbourne&amp;ie=UTF8&amp;hq=&amp;hnear=1%2F32+Fennell+St,+Port+Melbourne+Victoria+3207&amp;t=m&amp;ll=-37.826599,144.938164&amp;spn=0.015457,0.032701&amp;z=14&amp;output=embed&amp;iwloc=near"></iframe>');
			} else {
				// progress through part 1
				$nextSlide.show();
			}
			$("html, body").animate({
				scrollTop: 0
			}, "slow");
		},
		// API call when IE9 or earlier to avoid cross domain issues related to $.getJSON
		xDomainRequest = function($resultsPage) {
			var xdr;
			// run function
			req_init();

			function req_init() {
				var url = getStyleGroup(categoryId, group);

				if (window.XDomainRequest) {
					xdr = new XDomainRequest();
					if (xdr) {
						// When the object is complete, the onload event is raised & the responseText ensures the data is available.
						xdr.open("get", url);
						//xdr.timeout = '5000';
						xdr.onload = function () {
							buildStyleList(jQuery.parseJSON(xdr.responseText), $resultsPage);
						};
						xdr.onprogress = function() {};
						xdr.ontimeout = function () {};
						xdr.onerror = function () {};
						setTimeout(function(){
							xdr.send();
						}, 500);
					} else {
						alert('Failed to create new XDR object.');
					}
				} else {
					alertDisp('XDR does not exist.');
				}
			}
		},
		// called on answer clicked - add/subtracts points to p1/p2 answers
		answerCounter = function($answer, tallyControl) {
			if ($answer.attr('data-category')) {
				// IF data-category, then p1
				voteCategory = $answer.attr('data-category');

				if (tallyControl === 'add') {
					if (voteCategory === 'classic') {
						classic = classic + 1;
					} else if (voteCategory === 'contemporary') {
						contemporary = contemporary + 1;
					} else if (voteCategory === 'designer') {
						designer = designer + 1;
					} else if (voteCategory === 'resort') {
						resort = resort + 1;
					}
				} else if (tallyControl === 'subtract') {
					if (voteCategory === 'classic') {
						classic = classic - 1;
					} else if (voteCategory === 'contemporary') {
						contemporary = contemporary - 1;
					} else if (voteCategory === 'designer') {
						designer = designer - 1;
					} else if (voteCategory === 'resort') {
						resort = resort - 1;
					}
				}
				//console.log('classic (' +classic +') ' +'contemporary (' +contemporary +') ' +'designer (' +designer +') ' +'resort (' +resort +')');
			} else if ($answer.attr('data-group')) {
				// IF data-group, then p2
				voteGroup = $answer.attr('data-group');

				if (tallyControl === 'add') {
					if (voteGroup === '1') {
						group1 = group1 + 1;
					} else if (voteGroup === '2') {
						group2 = group2 + 1;
					} else if (voteGroup === '3') {
						group3 = group3 + 1;
					} else if (voteGroup === '4') {
						group4 = group4 + 1;
					}
				} else if (tallyControl === 'subtract') {
					if (voteGroup === '1') {
						group1 = group1 - 1;
					} else if (voteGroup === '2') {
						group2 = group2 - 1;
					} else if (voteGroup === '3') {
						group3 = group3 - 1;
					} else if (voteGroup === '4') {
						group4 = group4 - 1;
					}
				}
				//console.log('group1 (' +group1 +') ' +'group2 (' +group2 +') ' +'group3 (' +group3 +') ' +'group4 (' +group4 +')');
			}
		},
		// called on result slides @ end of part 1/2 to total answers
		tallyAnswers = function(whichPart) {

			if (whichPart === 'part1') {
				mostVotes = Math.max(classic, contemporary, designer, resort);

				if (classic === mostVotes) {
					category = 'classic';
				} else if (contemporary === mostVotes) {
					category = 'contemporary';
				} else if (designer === mostVotes) {
					category = 'designer';
				} else if (resort === mostVotes) {
					category = 'resort';
				}
			} else if (whichPart === 'part2') {
				mostVotes = Math.max(group1, group2, group3, group4);

				if (group1 === mostVotes) {
					group = 'group1';
				} else if (group2 === mostVotes) {
					group = 'group2';
				} else if (group3 === mostVotes) {
					group = 'group3';
				} else if (group4 === mostVotes) {
					group = 'group4';
				}
			}
		},
		// determine progress bar fill according to quiz progress
		progressBar = function($nextSlide) {
			// calculate total slides, part 1 + part 2 + 1 = total slides, part 2 / 4 because x4 duplication, +1 = results page
			part1 = $wosQuiz.find('.module-wrap[data-quiz="part1"]').length;
			part2 = $wosQuiz.find('.module-wrap[data-category]').length / 4;
			totalSlides = part1 + part2 + 1;
			// figure out where we're at, -1 for start slide
			currentSlide = $nextSlide.attr('data-progress') - 1;
			progressPercent = (currentSlide / totalSlides) * 100;
			// update next slide's progress bar
			$nextSlide.find('.wosq-progress-highlight').css('width', progressPercent + '%');
		},
		// build style results grid on results page
		buildStyleList = function(data, $resultsPage) {
			style = data.InteriorStyles;
			//console.log(style[1].Description);

			for (var i = 0; i < style.length; i++) {
				list  = '<li>';
				list += '<div class="wosq-style">';
				list += '<a href="' + website + style[i].ItemURL + '" class="wosq-title">' + style[i].Title + '</a>';
				list += '</div>';
				list += '<a href="' + website + style[i].ItemURL + '" class="wosq-select">';
				list += '<img src="' + website + style[i].ImageURL + '" alt="' + style[i].Title + '">';
				list += '<div class="wosq-description">';
				list += '<span>' + style[i].Description + '</span>';
				list += '</div>';
				list += '</a>';
				list += '</li>';

				// append results & remove results loader class
				$resultsPage.find('ul.wosq-grid-result').removeClass('wosq-loader').append(list);
			}
		},
		// update social media links with proper to-share content
		updateSocialMediaLinks = function() {
			var $sharebox = $('#wosq-smedia'),
				$twitter = $sharebox.find('a.wosq-twitter'),
				$facebook = $sharebox.find('a.wosq-facebook'),
				$email = $sharebox.find('a.wosq-email'),
				shareTitle = $sharebox.attr('data-sm-title'),
				image = $sharebox.attr('data-sm-image'),
				redirectLink = encodeURIComponent(document.location.protocol + "//" + document.domain),
				emailLink = encodeURIComponent(document.location.protocol +'//' +document.domain +'?cat=' +category +'&catId=' +categoryId +'&group=' +group),
				fbAppID = $sharebox.attr('data-sm-fbappid'),
				link = encodeURIComponent(document.location.protocol + "//" + document.domain),
				shareDescription = null,
				twitterLink = null,
				facebookLink = null,
				lineBreak = '%0A%0A';

			image = encodeURIComponent(document.location.protocol + "//" + document.domain + image);
			twitterLink = "http://twitter.com/intent/tweet?original_referer=" + link + "&text=" + shareDescription + "&url=" + link;
			facebookLink = "https://www.facebook.com/dialog/feed?app_id=" + fbAppID + "&link=" + link + "&picture=" + image + "&name=" + shareTitle + "&caption=Porter Davis Homes&description=" + shareDescription + "&redirect_uri=" + redirectLink;

			shareDescription  = "Hi," + lineBreak;
			shareDescription += "I just did a great quiz to find out my interior style. Here's my results – see if you think these styles would suit me. " + emailLink + "." + lineBreak;
			shareDescription += "If you want to do the quiz go to " + link + " or click on Start Over from the link above." + lineBreak;
			shareDescription += "There's great photos on the Porter Davis website – perfect for interior style ideas and inspiration. They have 64 different Worlds of Style across Resort, Designer, Classic and Contemporary themes.";

			$twitter.attr("href", twitterLink).attr("target", "_blank");
			$facebook.attr("href", facebookLink).attr("target", "_blank");
			$email.attr("href", "mailto:?subject=" + shareTitle + "&body=" + shareDescription);
		},
		// API Call
		getStyleGroup = function(categoryId, group) {
			return website + Services.styleresults + 'GetWorldofStyleQuizImages?category=' + categoryId + '&group=' + group + '&seed=undefined';
		};

	// exposed functions
	return {
		init: init
	};

})(document, jQuery);
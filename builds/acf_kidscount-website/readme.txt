	// TOC
	-------------------------------------------------------------------------------

	1.= Deployment Checklist
	2.= Plugin Use
	3.= HTML
    4.= Image Naming Conventions
    5.= Server Side Includes  
    6.= Robots!
    
    
	1.= Deployment Checklist
	-------------------------------------------------------------------------------
	Before any code goes live it should pass the following checklist:

	* Have the plugins been compiled into the plugin.js file to ensure minimal http requests?
    * Are skiplinks present on the site?
    * Is the site valid?
    * Is Jquery loaded from Googles CDN?
    
    
	2.= Plugin Use
	-------------------------------------------------------------------------------
	Plugins, their support files and their src (basically everything in the plugin
	zip file) should be placed in it's own subfolder. When the site is deployed
	all plugins should be compiled into plugins.js and compressed to cut down the
	number of http requests required when loading pages.

	3.= HTML
	-------------------------------------------------------------------------------
        
        Skip To Block
        -------------
        All pages should contain a skip to / skipLinks block that is positioned off
        the screen instead of display : none;
	
        JavaScript
        ----------	
        jQuery is the preferred DOM library and is loaded from Googles CDN with a local fallback.
        
        Page specific client code is handled in the vj-global file. To create code
        that is specific for a page on the site, create a method in the vj global
        client singleton then execute it on the page within a $(document).ready();
        See the homepage for an example of how to do this.
    
    4.= Image Naming Conventions
	-------------------------------------------------------------------------------   
    * Bullets: bul_[colour or type].[png, jpg, gif]
    * Button: btn_[name].[png, jpg, gif]
    * Nav: nav_[type of nav i.e. primary, secondary, ]
    * Standard images:  img_[name].[png, jpg, gif]
    * Header images: hd_[name of header].[png, jpg, gif]
    * Blockequotes: quote_[name].[png, jpg, gif]
    * Icons: icon_[name].[png, jpg, gif]
    
    5.= Server Side Includes
	-------------------------------------------------------------------------------    
    If you are using server side includes change all .html files to .shtml

    Name your incluides as follows:
        _inc_header.shtml
        _inc_nav.shtml
        _inc_footer.shtml

        ect...

    Add includes to your pages as follows:
        <!--#include file="_inc_header.shtml" -->

    6.= Robots!
    -------------------------------------------------------------------------------
    When deploying to staging ensure that robots.txt is present and it contains

    User-agent: *
    Disallow: /

    This is imperitive to ensure that staging urls are not index by search engines.

    It should be there by default on staging sites set up by I.T. just ensure you
    don't remove it.
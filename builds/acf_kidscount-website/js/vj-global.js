/*jslint devel: true, sloppy: true, maxerr: 50, indent: 4 */
var ACF = ACF || {};

ACF.kidsCount = (function () {

	jCycle = function () {
		// home page hero carousel
		var $slider1 = $('#slider1');
		if ($slider1.length === 0) return;

		$slider1.cycle({ 
			fx:     'scrollHorz',
			prev:   '#prev1',
			next:   '#next1',
			pause: true,
			timeout: 2000 
		});

		$.extend($.easing,{def:'easeInOutExpo',swing:function(x,t,b,c,d){return $.easing[$.easing.def](x,t,b,c,d);},easeInOutExpo: function(x,t,b,c,d){if(t==0) return b;if(t==d) return b+c;if((t/=d/2)<1) return c/2 * Math.pow(2,10*(t-1))+b;return c/2 * (-Math.pow(2,-10*--t)+2)+b;}});
	},
	contactForm = function () {
		// contact form functions & validation
		var $contactForm = $('#contactForm');
		if ($contactForm.length === 0) return;

		$('#form1').validate({
			messages: {
				txtFirstname: 	{ required: 'Please enter your first name' },
				txtLastname: 	{ required: 'Please enter your last name' },
				txtMessage: 	{ required: 'Please enter your message' },
				txtEmail: 		{ required: 'Please enter your email' },
				txtPassword: 	{ required: 'Please enter your password' },
				txtPhone:  		{ required: 'Please enter your phone number'}
			}
		});

		/*Setting phone to hidden while keeping block display as default*/
		$('.phoneSelected', $contactForm).toggle();
		/*Toggling preferred contact*/
		$('#preferredEmail, #preferredPhone', $contactForm).change(function() {
			$('.emailSelected', $contactForm).toggle();
			$('.phoneSelected', $contactForm).toggle();
		});
	},
	listSwitch = function () {
		// phone directory list switch
		var $phoneDir = $('#phone-directory');
		if ($phoneDir.length === 0) return;

		var $stateList = $phoneDir.find('.phone-listing'),
			$stateBtn = $phoneDir.find('ul.list-sel a');

		$stateBtn.click(function (event) {
			var btnClicked = this.id,
				$this = $(this);
			
			$("."+btnClicked).slideDown().siblings('div:visible').slideUp();
			$this.closest('.list-sel').find('.button').removeClass('active');
			$this.addClass('active');
			event.preventDefault();
		});

		$stateBtn.first().trigger('click');
	},
	breadcrumbsLink = function () {
		// content pages' breadcrumbs
		var $breadcrumbs = $('#ddlBreadcrumbs');
		if ($breadcrumbs.length === 0) return;

		$breadcrumbs.change(function () {
			window.location = this.value;
		});
	},
	printBtn = function () {
		//print button
		var $printBtn = $('#print-btn');
		if ($printBtn.length === 0) return;

		$printBtn.on('click', function (e) {
			window.print();
			e.preventDefault();
		});
	},
	attachHovers = function () {
		// box links info slide in/out
		var $boxlinks = $('#middle').find('a.slide-link');
		if ($boxlinks.length === 0) return;

		$boxlinks.on({
			mouseenter: function() {
			$($(this).children('.slide-module'))
				.stop().animate({bottom: 0, left: 0}, 200);
			},
			mouseleave: function() {
			$($(this).children('.slide-module'))
				.stop().animate({bottom: -234, left: 0}, 150);
				// used 234px to fix weird display bug in chrome where the slide stops 1px short of full cover
			}
		});
	},
	audioPlayer = function () {
		// audio module
		var $audioModule = $('#audio-module');
		if ($audioModule.length === 0) return;

		$("#jplayer").jPlayer({
			ready: function () {
				$(this).jPlayer("setMedia", {
					mp3: "/builds/acf_kidscount-website/audio/eng16.mp3"
				});
			},
			swfPath: "/js",
			supplied: "mp3",
			preload: "auto",
			volume: 1,
			cssSelectorAncestor: "",
			cssSelector: {
				play: "#jp-play",
				pause: "#jp-pause",
				seekBar: "#jp-seek-bar",
				playBar: "#jp-play-bar",
				currentTime: "#jp-current-time",
				mute: "#jp-mute",
				unmute: "#jp-unmute",
				noSolution: "#jp-no-solution"
			}
		});
	},
	textInputClear = function () {
		// to clear the default values on text fields when focused
		var $textField = $('input.text-field');
		if ($textField.length === 0) return;

		$.fn.clearOnFocus = function () {
			this.blur(function () {
				if (!this.value) {
					this.value = this.defaultValue;
				}
			}).focus(function () {
				if (this.value == this.defaultValue) {
					this.value = '';
				}
			});
		};
		$textField.clearOnFocus();
	},
	datePicker = function () {
		// search news articles module calendar plugin for date range select
		if ($('#search-news').length === 0) return;

		var $dateFrom = $('#dateFrom'),
			$dateTo = $('#dateTo');

		$dateFrom.datepicker({
			onSelect: function(selectedDate) {
				$dateTo.datepicker("option","minDate",selectedDate);
			}, dateFormat: 'dd/mm/yy'
		});
		$dateTo.datepicker({
			onSelect: function( selectedDate ) {
				$dateFrom.datepicker("option","maxDate",selectedDate);
			}, dateFormat: 'dd/mm/yy'
		});
	}

	// public functions
	return {
		homePage: function () {
			jCycle();
			attachHovers();
			textInputClear();
		},
		contentPages: function () {
			breadcrumbsLink();
			printBtn();
			attachHovers();
			listSwitch();
			contactForm();
			audioPlayer();
			textInputClear();
			datePicker();
		}
	};
}());
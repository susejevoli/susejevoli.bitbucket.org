/*jslint devel: true, sloppy: true, maxerr: 50, indent: 4

	ISSUES
	1. mobile landscape mode - scrolling up/down non-full screen causes clipping of bottom half of video (weird)

*/

var MYAPP = MYAPP || {};

MYAPP.namespace = (function () {

	var viewportHeight = document.documentElement.clientHeight,
		videoHeight = null,
		$btnSlide = $('#btn-slide');

	slideAnimate = function () {

		var videoID = null;

		$btnSlide.after('<div id="cover-slide" class="slide"></div>');

		$('#joy-btn').on('click', function (e) {
			e.preventDefault();

			videoID = $(this).data('videoid');
			$btnSlide.css('top', '5%').addClass('animateDown');

			setTimeout(function () {
				$btnSlide.removeClass('animateDown').css('top', '-100%').addClass('animateUp');
			}, 500);

			setTimeout(function () {
				$('#cover-slide').hide();
				$('#video-block').html('<iframe width="640" height="360" src="http://www.youtube.com/embed/' +videoID +'?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>');
			}, 600);

		});

		$('#back-btn').on('click', function (e) {
			e.preventDefault();

			$btnSlide.removeClass('animateUp').css('top', '0').addClass('animateBack');

			setTimeout(function () {
				$('#cover-slide').show();
				$btnSlide.removeClass('animateBack');
				$('#video-block').empty();
			}, 1000);

		});

	},

	videoValign = function (videoHeight, viewportHeight) {
		
		if (viewportHeight > videoHeight) { 
			var vidTopPadding = Math.round(((viewportHeight - videoHeight)/2)*0.8);
			$('#vid-slide').css('padding-top', vidTopPadding);
		} else {
			$('#vid-slide').css('padding-top', '0');
		}
		
	},

	currentViewportWidth = function () {
		$('#viewport-width').html(document.documentElement.clientWidth);
	},

	checkOrientation = function (viewportWidth, viewportHeight) {

		if (viewportWidth < 1025) {

			if (viewportHeight > viewportWidth) {
				$('#vid-slide').addClass('vertical-viewport');
			} else if (viewportWidth > viewportHeight && $('#vid-slide').hasClass('vertical-viewport')) {
				$('#vid-slide').removeClass('vertical-viewport');
			}

		} else if (viewportWidth > 1024 && $('#vid-slide').hasClass('vertical-viewport')) {

			$('#vid-slide').removeClass('vertical-viewport');

		}
	}

	// public functions
	return {

		init: function () {

			var viewportHeight = document.documentElement.clientHeight,
				viewportWidth = document.documentElement.clientWidth,
				videoHeight = null;

			slideAnimate();
			currentViewportWidth();
			checkOrientation(viewportWidth, viewportHeight);

			$(window).on({
				load: function(){
					videoHeight = $('#vid-slide').height();
					videoValign(videoHeight, viewportHeight);
				},
				resize: function(){
					viewportHeight = document.documentElement.clientHeight;
					viewportWidth = document.documentElement.clientWidth;
					videoHeight = $('#vid-slide').height();
					videoValign(videoHeight, viewportHeight);
					currentViewportWidth();
					checkOrientation(viewportWidth, viewportHeight);
				}
			});

		}

	};

}());
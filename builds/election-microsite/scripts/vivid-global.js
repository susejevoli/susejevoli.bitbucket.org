/*jslint devel: true, sloppy: true, maxerr: 50, indent: 4 */

var EMICRO = EMICRO || {};

EMICRO.namespace = (function () {

	jCycle = function () {
		var $context = $('#carousel-context');
		if (($context.length == 0) || $context.size() == 0) return;

		$('.carousel-1', $context).cycle({ 
			fx:     'scrollHorz',
			prev:   '.left-1',
			next:   '.right-1',
			pause: true,
			timeout: 2000 
		});
		$('.carousel-2', $context).cycle({ 
			fx:     'scrollHorz',
			prev:   '.left-2',
			next:   '.right-2',
			pause: true,
			timeout: 2000 
		});
		$('.carousel-3', $context).cycle({ 
			fx:     'scrollHorz',
			prev:   '.left-3',
			next:   '.right-3',
			pause: true,
			timeout: 2000 
		});
	},
	scrollBar = function(){
		$('.scroll-panel').tinyscrollbar();
	},
	percentageSupport = function(){
		/* CALCULATE PERCENTAGE BY LOADING PAGE */
		$(".support-bar-percentage").each(function(){
			var currentPercentage = parseFloat($(this).children("span").text().split("%")),
				updatedWidthBar = currentPercentage * 2 + 2;
				
			$(this).children("div").css({"width" : ""+ updatedWidthBar +"px"});
		});
	},
	resizeHighestDiv = function () {
		var $context = $('#carousel-context');
		if (($context.length == 0) || $context.size() == 0) return;

		var maxHeight = 0;
		$('.bottom', $context)
			.each(function() { maxHeight = Math.max(maxHeight, $(this).height()); })
			.height(maxHeight);
	}

	return {

		jCycle: function () { jCycle(); },
		scrollBar: function () { scrollBar(); },
		percentageSupport: function () { percentageSupport(); },
		resizeHighestDiv: function () { resizeHighestDiv(); },

	};

}());

$.extend($.easing,{def:'easeInOutExpo',swing:function(x,t,b,c,d){return $.easing[$.easing.def](x,t,b,c,d);},easeInOutExpo: function(x,t,b,c,d){if(t==0) return b;if(t==d) return b+c;if((t/=d/2)<1) return c/2 * Math.pow(2,10*(t-1))+b;return c/2 * (-Math.pow(2,-10*--t)+2)+b;}});
/*jslint devel: true, sloppy: true, maxerr: 50, indent: 4 */
var ACF = ACF || {};

ACF.namespace = (function () {

	inputPrompt = function () {
		$.fn.clearOnFocus = function () {

	        this.blur(function () {
	            if (!this.value) {
	                this.value = this.defaultValue;
	                $(this).addClass('inputPrompt');
	            }
	        }).focus(function () {
	            if (this.value == this.defaultValue) {
	                this.value = '';
	                $(this).removeClass('inputPrompt');
	            }
	        });
	    };
	    $('input[type="text"]').clearOnFocus();
	    if (!('input[type="text"]').value) {
            $(('input[type="text"]')).addClass('inputPrompt');
        }
	},
	jCycle = function () {
		$('#slider1').cycle({ 
			fx:     'scrollHorz',
			prev:   '#prev1',
			next:   '#next1',
			pause: true,
			timeout: 2000 
		});
		
		$('.moduleBubble').cycle({ 
			fx:     'fade',
			prev:   '.moduleLeftArrow',
			next:   '.moduleRightArrow',
			cleartype: true,
      		cleartypeNoBg: true,
			speed: 500,
			pause: true,
			timeout: 6000 
		});
		$('#partnersCarousel').cycle({ 
			fx:     'fade',
			prev:   '#prev2',
			next:   '#next2',
			speed: 1000,
			pause: true,
			timeout: 200000 
		});
	},
	jCarousel = function () {
		$('#donationsCarousel').jcarousel({			
			auto: 3,			
			scroll: 1,	
			wrap: 'circular',
			buttonNextHTML: 'null',
			buttonPrevHTML: 'null'
		});
	},
	/* Attaches rollover animation to small navigation boxes. */
	attachHovers = function () {
		$('a.slide-link').live('mouseenter', function() {
			$($(this).children('.slide-module'))
				.stop().animate({bottom: 0, left: 0}, 200);
		}).live('mouseleave', function() {
			$($(this).children('.slide-module'))
				.stop().animate({bottom: -233, left: 0}, 150);
		});
		$('a.slide-link.side').live('mouseenter', function() {
			$($(this).children('.slide-module'))
				.stop().animate({top: 0, left: 0}, 200);
		}).live('mouseleave', function() {
			$($(this).children('.slide-module'))
				.stop().animate({top: 0, left: 290}, 150);
		});
	},
	modalPopup = function () {
		$('#registerBtn').click(function(event) {
			$('.modalWrapper').fadeIn(400);
			event.preventDefault();	
		});

		$('.overlay, #registerClose').click(function(event) {
			$('.modalWrapper').fadeOut(400);
			$('.registerModal input.field, .registerModal textarea').val('');
			$('.error').removeClass('error');
			event.preventDefault();
		});
	},
	datePickerUi = function () {
		$dpContext = $('ul.form');
		if (($dpContext.length == 0) || ($dpContext.size == 0)) return;
		$('#dateFrom', $dpContext).datepicker({
			onSelect: function( selectedDate ) {
				$("#dateTo", $dpContext).datepicker("option","minDate",selectedDate);
			}, dateFormat: 'dd/mm/yy'
		});
		$('#dateTo', $dpContext).datepicker({
			onSelect: function( selectedDate ) {
				$("#dateFrom", $dpContext).datepicker("option","maxDate",selectedDate);
			}, dateFormat: 'dd/mm/yy'
		});
	},
	quoteAlign = function () {
		$('.quoteText span').each( function (i, e) {
			$(this).parent().parent().show();
			var distance = $(this).height()/2;
			$(this).css({
				opacity: 1,
				marginTop: -distance+'px'
			});
		});
	},
	drawMap = function() {
		if($('#paper')[0]) {
	        var R = Raphael("paper", 225, 214);
	        var attr = {
	            fill: "#e2136d",
	            stroke: "#e8e8e8",
	            "stroke-width": 2,
	            cursor: "pointer"
	        };

	        var lineAttr = {
	            stroke: "#D3E1EB",
	            "stroke-width": 2
	        };

	        var actAttr = {
	            fill: "#e2136d",
	            stroke: "#e8e8e8",
	            "stroke-width": 2,
	            cursor: "pointer"
	        };

	        var aus = {};

	        aus.N = R.path("M152.047,147.095c0.784,0.313,5.139,2.051,6.629,2.61c1.64,0.615,3.278,3.894,4.508,5.328 c1.229,1.435,3.688,4.508,5.329,5.123c1.639,0.615,5.06,2.043,7.929,1.634c2.868-0.409,5.187-1.634,7.851,1.44 c2.613,3.016,7.395,8.785,10.477,10.355c0.412-0.344,0.682-0.863,0.761-1.646c0.437-4.359,8.021-18.829,9.852-20.747 s1.046-4.271,1.831-5.405c0.784-1.133,2.004-0.349,3.225-0.784c1.22-0.436,3.662-6.189,6.103-10.287 c2.441-4.097,5.317-12.902,5.491-17.086c0.002-0.027-0.001-0.057,0-0.084c-1.227,0.045-5.178,0.19-6.385,0.19 c-1.435,0-2.459,1.64-2.869,3.074c-0.41,1.435-1.845,2.869-2.869,2.869c-1.025,0-6.148-6.558-9.223-6.968 c-3.073-0.41-5.941,2.869-5.941,2.869l-42.625-4.508L152.047,147.095z").attr(attr);
	        aus.X = R.path("M87.327,98.179c0,0,33.095-1.338,52.255-1.134l-0.298-60.859c-4.068-2.901-9.239-5.431-12.132-7.239 c-3.487-2.179-2.528-3.399-2.528-5.056c0-1.656,2.702-5.927,3.4-6.712c0.698-0.785,1.569-0.524,2.354-1.308 c0.785-0.784,0.611-2.615,1.133-3.487c0.523-0.872,1.831-1.395,0.61-2.615c-1.221-1.221-3.748-0.959-3.748-0.523 c0,0.436-0.437,1.657-0.785,1.657c-0.349,0-1.221-0.35-1.57-0.959c-0.349-0.61-1.743-1.133-3.051-1.133s-1.744,1.046-2.615,1.046 s-1.308-0.523-1.831-1.046c-0.523-0.522-2.092-0.349-3.662-0.349s-3.659-1.781-4.969-2.354c-1.31-0.573-3.493,1.303-3.487,2.267 c0.006,0.964-2.537,2.514-4.106,2.428c-1.569-0.087-3.129,0.013-4.785,0.013c-1.657,0-5.144,3.923-5.056,5.057 c0.087,1.133-0.088,3.313-0.436,3.574s-0.785-0.697-1.917-0.523c-1.133,0.174-2.442,3.313-3.052,4.358 c-0.61,1.045-0.348,1.918,1.569,2.616c1.918,0.697,2.092,2.615,1.133,3.661c-0.958,1.046-2.777-1.196-5.218-1.283L87.327,98.179z").attr(attr);
	        aus.Q = R.path("M152.391,115.758l42.625,4.508c0,0,2.868-3.279,5.941-2.869c3.074,0.41,8.197,6.968,9.223,6.968 c1.024,0,2.459-1.435,2.869-2.869c0.41-1.434,1.435-3.074,2.869-3.074c1.207,0,5.158-0.145,6.385-0.19 c0.154-4.171-1.133-9.372-1.133-15.694c0-6.364-3.225-8.456-6.451-10.548c-3.225-2.092-3.399-6.364-2.963-7.584 c0.436-1.221,1.482-4.62,0.609-5.405c-0.871-0.785-2.004,2.354-3.051,2.528c-1.045,0.174-1.221-3.139-1.918-3.225 c-0.697-0.087-0.785,2.79-1.656,2.876s-1.744-1.308-1.744-3.574c0-2.266-0.579-5.292-0.579-5.292 c-2.05-1.64-2.05-4.509-1.229-5.328c0.819-0.819,1.639-2.049,0.41-2.049c-1.23,0-3.689,0.41-5.738-1.229 c-2.049-1.64-6.147-6.968-9.427-9.836c-3.279-2.87-0.409-6.558-0.819-8.197c-0.41-1.639-3.689-6.967-4.099-8.197 c-0.41-1.229,2.869-7.788,0.819-8.607c-2.049-0.819-2.049-2.049-3.688-3.688c-1.64-1.64-0.82,1.229-2.46,2.049 c-1.639,0.82-2.459-1.229-3.688-2.459s-2.05-9.016-2.459-10.656c-0.41-1.64,0-8.607-0.41-10.247C170.219,2.229,168.17,1,168.17,1 c-1.307,0.105-1.309,2.247-1.309,4.427c0,2.179-1.83,1.656-2.528,2.18c-0.698,0.523-1.221,2.092-1.221,3.748 s-0.348,2.005-0.871,3.313S161.718,26,161.718,27.831s-1.917,10.112-2.528,12.03c-0.61,1.918-4.097,4.882-7.062,5.144 c-2.964,0.261-6.015-2.964-10.199-6.625c-0.712-0.623-1.504-1.232-2.338-1.828l-0.483,60.351 c6.371,0.068,11.288,0.363,13.077,1.027L152.391,115.758").attr(attr);
	        aus.S = R.path("M88.547,134.086c2.559-1.024,5.456-1.984,8.354-1.984c4.097,0,7.672,0.349,8.805,0.959 c1.133,0.611,2.876,1.133,5.753,1.133s6.276,1.831,6.276,3.051s-3.225,1.744-0.61,3.662s4.446,4.01,4.446,5.404 c0,1.395,1.395,3.923,2.528,5.58c1.133,1.656,2.092-0.174,2.353-1.221c0.261-1.046,1.396-4.708,5.58-7.41 c4.184-2.702,1.743-6.015,3.662-6.015c1.918,0,1.569,3.836,1.569,5.928c0,2.093-1.831,6.451-3.4,8.02s-3.487,1.482-3.138,2.267 c0.349,0.785,2.789,0,4.01-0.611c1.221-0.61,1.482-4.01,3.836-3.225c2.353,0.785-0.785,6.189-0.087,6.8s3.226-2.703,4.359-2.354 s0.349,3.313,0.958,4.794c0.608,1.477,0.007,10.916,8.283,14.518l-0.07-26.288c-0.079-0.031-0.134-0.054-0.134-0.054 s0.055,0.022,0.134,0.054l0.968-50.531c-1.79-0.665-7.429,0.39-13.8,0.321l-51.853,0.975L88.547,134.086z").attr(attr);
	        aus.T = R.path("M175.052,193.573c-1.396,0-4.707,3.312-6.104,3.312 c-1.395,0-5.934-3.889-7.147-4.01c-1.744-0.174-1.047,2.964-1.134,3.835c-0.087,0.872,1.481,2.005,1.481,3.4 c0,1.395,0.697,4.01,1.047,5.057c0.348,1.046,4.881,6.538,5.84,6.625c0.959,0.087,2.703-2.964,3.227-3.574 c0.521-0.61,1.83-1.482,3.486-2.354c1.656-0.872,1.482-4.01,1.482-5.056s0.697-2.616,0.783-4.185 C178.103,195.055,176.446,193.573,175.052,193.573z").attr(attr);
	        aus.V = R.path("M185.393,164.357c-2.664-3.074-5.957-3.137-8.825-2.728c-2.869,0.41-6.451-0.858-8.09-1.473 c-1.641-0.615-3.438-4.634-4.667-6.068c-1.229-1.434-3.53-3.768-5.17-4.383c-1.49-0.559-5.845-2.297-6.629-2.61l0.071,26.288 c0.029,0.013,0.056,0.028,0.085,0.041c8.37,3.574,5.928,4.185,10.809,4.794c4.883,0.61,3.75-5.754,6.189-5.754 c2.441,0,1.744,2.964,5.666,6.364c3.924,3.4,4.883-1.046,9.416-3.661c3.717-2.145,8.605-0.013,10.484-1.58 C191.651,172.016,188.006,167.374,185.393,164.357z").attr(attr);
	        aus.W = R.path("M84.227,27.954c-2.44-0.087-2.075,3.612-3.761,3.462c-1.383-0.123-0.872-2.441-0.785-3.661 c0.087-1.221-0.436-1.918-1.395-3.051c-0.959-1.133-3.437-1.57-6.663-1.745c-3.226-0.175-4.826,0.834-4.303,2.229 s0.33,2.435-0.279,2.393c-1.258-0.087-1.104-2.7-2.527-2.703c-0.611-0.001,0.384,2.616-0.575,2.616s-2.205-1.111-2.459-0.077 c-0.48,1.953,2.248,1.908,2.248,2.605s-3.6-0.31-3.573,1.046c0.019,0.96,2.442,1.789,2.094,2.574c-0.349,0.785-3.12-0.175-3.381,0 s-0.753,0.752-0.23,1.537s-0.4,2.863-0.487,3.387c-0.087,0.522-2.078,0.169-3.125-0.005s-2.28-0.344-2.542,0.005 c-0.262,0.348-0.436,1.046,1.046,2.091c1.481,1.047,2.092,2.354,2.005,3.401c-0.087,1.046-1.657,2.615-2.615,2.702 c-0.959,0.087-2.093-2.005-2.79-4.707c-0.697-2.703-4.271-3.313-4.271,4.794s-2.703,8.979-4.185,11.943 c-1.482,2.964-4.533,4.794-6.015,5.318c-1.482,0.523-6.8,0-7.498,0.348c-0.697,0.349-0.61,2.441-1.22,2.441 c-0.611,0-2.615,0-3.662,0.785c-1.046,0.785-2.005,3.574-3.749,3.662c-1.743,0.086-2.702-2.093-4.271-2.093 s-2.615,3.662-3.575,4.882c-0.958,1.22-5.84,5.318-6.538,6.538C4.45,81.89,4.45,84.767,3.491,84.767 c-0.959,0-1.133-4.795-2.179-4.882c-1.046-0.088-0.087,6.886-0.087,8.369c0,1.481-1.308,4.62-1.22,6.974s1.917,6.015,3.574,7.41 c1.657,1.395,4.01,4.795,3.399,5.58c-0.61,0.785-1.918-0.523-1.918-0.523s-3.138-3.4-3.573-3.139 c-0.436,0.262,0.086,1.046,0.697,1.831c0.611,0.785,2.615,2.702,2.267,3.487c-0.348,0.785-1.831-0.523-2.963-0.872 c-1.134-0.349-1.657,0.174-0.959,1.046c0.698,0.872,4.969,4.358,6.8,5.231c1.831,0.872,1.481,1.656,1.481,4.01 s2.529,5.84,4.185,8.02c1.656,2.18,4.097,9.589,4.969,10.635s4.708,6.8,4.708,10.2c0,3.399-2.354,6.102-2.354,7.671 s2.703,2.615,7.585,4.533c4.882,1.917,7.846,0.523,8.979-0.174c1.133-0.697,1.569-4.01,2.005-4.882 c0.436-0.872,2.615-1.308,3.4-1.831s1.395-0.785,2.005-1.831c0.61-1.046,2.266-1.395,2.266-1.395s7.062-0.262,13.25-0.436 c6.189-0.174,4.708-3.487,4.708-4.794s1.395-2.267,2.441-2.354c1.045-0.087,2.353-0.698,4.358-2.79 c2.005-2.092,4.271-2.877,6.538-2.877s4.271-0.349,7.41-1.656c0.918-0.382,1.921-0.818,2.979-1.242L84.355,27.979l3.885,106.106").attr(attr);
	        aus.A = R.path("M193.167,147.536c2.513-1.499,8.477,2.382,2.255,5.85 c-4.499,2.508-1.201,3.666-5.218,4.456C186.189,158.631,186.858,151.304,193.167,147.536z").attr(attr);

	        var current = null;
	        var stateLabels = $('#paper div');

	        for (var state in aus) {
	            (function(st, state) {

	            	//disable hover for mobile - 1 click to link
	            	if($('html').hasClass('no-touch')) {
						$(st[0], stateLabels[0]).hover(function() {
						    if (!$(st[0]).data('active')) {
						        st.animate({ fill: "#e972a6" }, 200);
						        R.safari();
						    }
						}, function() {
						    if (!$(st[0]).data('active')) {
						        st.animate({ fill: "#e2136d" }, 200);
						        R.safari();
						    }
						});
					}

	                $(st[0]).click(function() {
						// identify what state clicked
						//alert(st.id);
						var whatState = st.id;
						switch (whatState) {
							case 0:
								alert('NSW clicked');
								window.location = '/seminar-list.shtml';
								break;
							case 1:
								alert('NT clicked');
								//window.location = '/seminar-list.shtml';
								break;
							case 2:
								alert('QLD clicked');
								//window.location = '/seminar-list.shtml';
								break;
							case 3:
								alert('SA clicked');
								//window.location = '/seminar-list.shtml';
								break;
							case 4:
								alert('TAS clicked');
								//window.location = '/seminar-list.shtml';
								break;
							case 5:
								alert('VIC clicked');
								//window.location = '/seminar-list.shtml';
								break;
							case 6:
								alert('WA clicked');
								//window.location = '/seminar-list.shtml';
								break;
							case 7:
								alert('ACT clicked');
								//window.location = '/seminar-list.shtml';
								break;
						}
	                });

	            })(aus[state], state);
	        }

		};
    },
	validation = function () {
		var $modalContext = $('body.homepage');
		$('#form1', $modalContext).validate({
			errorPlacement: function (error, element) {
				return true;
			}
		});		
		$('#form1').validate({
			messages: {
				txtFirstname: 	{ required: 'Please enter your first name' },
				txtLastname: 	{ required: 'Please enter your last name' },
				txtMessage: 	{ required: 'Please enter your message' },
				txtEmail: 		{ required: 'Please enter your email' },
				txtPassword: 	{ required: 'Please enter your password' },
				txtPhone:  		{ required: 'Please enter your phone number'}
			}
		});
		var $fContext = $('#contactForm.field-switcher');
		if (($fContext.length == 0) || ($fContext.size() == 0)) return;
		/*Setting phone to hidden while keeping block display as default*/
		$('.phoneSelected', $fContext).toggle();
		/*Toggling preferred contact*/
		$('#preferredEmail, #preferredPhone', $fContext).change(function() {
			$('.emailSelected', $fContext).toggle();
			$('.phoneSelected', $fContext).toggle();
		});
	},
	listSwitch = function () {
		var $context = $('.phone-list');
		if (($context.length == 0) || $context.size() == 0) return;
		$('.qld-list', $context).toggle();
		$('.nsw-list', $context).toggle();
		$('.act-list', $context).toggle();
		$('.vic-list', $context).toggle();
		$('.tas-list', $context).toggle();
		$('.sa-list', $context).toggle();
		$('.wa-list', $context).toggle();
		$('.nt-list', $context).toggle();
		// #nat-list-btn, #qld-list-btn, #nsw-list-btn, #act-list-btn, #vic-list-btn, #tas-list-btn, #sa-list-btn, #wa-list-btn, #nt-list-btn
		$(".list-sel a", $context).click(function (event) {
		    var btnClicked = this.id;
		    //alert(btnClicked);
		    $("."+btnClicked, $context).slideDown().siblings('div:visible').slideUp();
		    $(this).closest('.list-sel').find('.button').removeClass('active');
		    $(this).addClass('active');
		    event.preventDefault();
		});
	},
	breadcrumbsLink = function () {
		var $bcContext = $('.breadcrumbs');
		if (($bcContext.length == 0) || ($bcContext.size() == 0)) return;
		$('#ddlBreadcrumbs', $bcContext).change(function () {
			window.location = this.value;
		});
	}

	// public functions
	return {
		init: function () {
			inputPrompt();
			jCycle();
			jCarousel();
			attachHovers();
			modalPopup();
			datePickerUi();
			quoteAlign();
			drawMap();
			validation();
			listSwitch();
			breadcrumbsLink();
		}
	};
}());

$.extend($.easing,{def:'easeInOutExpo',swing:function(x,t,b,c,d){return $.easing[$.easing.def](x,t,b,c,d);},easeInOutExpo: function(x,t,b,c,d){if(t==0) return b;if(t==d) return b+c;if((t/=d/2)<1) return c/2 * Math.pow(2,10*(t-1))+b;return c/2 * (-Math.pow(2,-10*--t)+2)+b;}});
/**
 * Colorado Interactive directive
 * TTrinh 9.2.15
 * HOL-24669 Colorado MY15 TVC - interactive version
 */
(function () {
	"use strict";

	var app = angular.module('app', []);
	
	app
	/**
	 * Colorado Interactive directive
	 * Animates mud, then loads jqueryEraser so user can remove mud
	 */
	.directive('coloradoInteractive', [function () {
		return {
			restrict: 'A',
			link: function (scope, iElement, iAttrs) {

				var coloradoDirty = iElement.find('.col-interactive__mud--final');

				// coloradoDirty doesn't exist for IE8, IE8 check
				if (!coloradoDirty.length) { return; }

				console.log('col-interactive', 'coloradoInteractive init!');

				$('<img />')
					.attr('src', '/builds/holden_colorado-interactive/images/mud-sprite.png')
					.load(function () {
						// ensure sprite is loaded before starting animation
						console.log('col-interactive', 'sprite loaded');

						var
							mud1 = iElement.find('.col-interactive__mud--1'),
							mud2 = iElement.find('.col-interactive__mud--2'),
							mud3 = iElement.find('.col-interactive__mud--3'),
							mud4 = iElement.find('.col-interactive__mud--4'),
							mud = iElement.find('div.col-interactive__mud'),
							instructions = iElement.find('.col-interactive__instructions'),
							refreshBtn = iElement.find('.col-interactive__refresh-btn'),
							testFunction = function () {
								console.log('col-interactive', 'testFunction');
							};

						// on complete: hide animated elements, load eraser.js
						function loadEraser() {
							console.log('col-interactive', 'coloradoInteractive load eraser!');

							// once-off canvas interaction tracking
							function cleaningInteraction() {
								console.log('col-interactive', 'once-off canvas interaction tracking');
								// Tracking.push({ 'event': 'ColoradoTruckCleaningInteraction' });

								// enable mud refresh button
								refreshBtn
									.removeClass('hide')
									.unbind( 'click')
									.on('click', function () {
										// 2. find coloradoDirty-hidden (1), assign var
										coloradoDirty = iElement.find('img.col-interactive__mud--final');

										// 3. make a copy of coloradoDirty-hidden (1) > (2)
										coloradoDirty.clone().insertAfter(coloradoDirty);

										// 4. fade coloradoDirty-hidden (1) in, addClass opacity-1
										coloradoDirty.removeClass('hide');
										TweenMax.to(coloradoDirty, 0.5,
											{css: {opacity: 1}, onComplete: testFunction() });

										setTimeout(function () {
											// 5. remove canvas, convert (1) to canvas
											iElement.find('canvas').remove();
											coloradoDirty.eraser({
												size: 50,
												completeRatio: 0.01,
												completeFunction: cleaningInteraction
											});

											// 6. hide refreshBtn, change cursor
											iElement.find('canvas').addClass('opacity-1 col-interactive--cursor');
											refreshBtn.addClass('hide');
										}, 500);

									});
							}

							coloradoDirty.addClass('opacity-1'); // needed to make canvas visible
							mud.addClass('hide');

							// load eraser, adjust eraser size, fire tracking
							coloradoDirty.eraser({
								size: 50,
								completeRatio: 0.01,
								completeFunction: cleaningInteraction
							});

							// change cursor
							iElement.find('canvas').addClass('col-interactive--cursor');
							instructions.addClass('col-interactive--cursor col-interactive__instructions--animation');
						}

						// 1. make a copy of coloradoDirty before canvas convert, hide it
						coloradoDirty.clone().insertAfter(coloradoDirty).addClass('hide');

						// animate mud
						TweenMax.to(mud1, 0.5, {css: {opacity: 1}});
						TweenMax.to(mud2, 0.5, {css: {opacity: 1}, delay: 0.25});
						TweenMax.to(mud3, 0.5, {css: {opacity: 1}, delay: 0.5});
						TweenMax.to(mud4, 0.5, {css: {opacity: 1}, delay: 0.75});
						TweenMax.to(coloradoDirty, 0.5, {css: {opacity: 1}, delay: 1});
						TweenMax.to(instructions, 0.5, {css: {opacity: 1}, delay: 1.25,
							onComplete: loadEraser});
					});

			}
		};
	}]);
	
})();
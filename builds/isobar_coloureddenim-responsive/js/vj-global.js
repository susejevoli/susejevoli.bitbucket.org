/*jslint devel: true, sloppy: true, maxerr: 50, indent: 4 */

var CSTEMPLATE = CSTEMPLATE || {};

CSTEMPLATE.namespace = (function () {

	var $html = $('html'),
		$body = $('body'),
		$qGallery = $('#quick-gallery'),
		$fbLink = $qGallery.find('.fancy-box'),
		$navLinks = $('#header').find('a'),

	viewportWidth = function () {
		$('#viewport-width').html(document.documentElement.clientWidth);
	},

	galleryHover = function () {

		$qGallery.on('mouseenter', 'li', function (event) {

			var $selected = $(this),
				$other    = $selected.siblings().not($selected),
				$link     = $selected.find('.thumbOver');

			$link.stop().animate({ opacity:  0.7 }, 250);
			$other.stop().animate({ opacity: 0.33 }, 250);

		}).on('mouseleave', 'li', function (event) {

			var $selected = $(this),
				$other    = $selected.siblings().not($selected),
				$link     = $(this).find('.thumbOver');
			
			$link.stop().animate({ opacity:  0 }, 150);
			$other.stop().animate({ opacity: 1 }, 150);

		});
	},

	scrollLink = function () {

		$navLinks.click(function() {
			var dest = $(this).data('link');
			$html.add($body).animate({scrollTop: $(dest).offset().top}, 400 );
			return false;
		});

	},

	lightBox = function () {

		// check if desktop or touch device, then use gallery functionality accordingly
		// desktop = fancyBox 2, touch device = PhotoSwipe
		if ($html.hasClass('no-touch')) {
			$fbLink.fancybox({
				padding: 0
			});
		} else { 
			// Set up PhotoSwipe with all anchor tags in the gallery container
			var options = {},
			instance = window.Code.PhotoSwipe.attach( $('#quick-gallery a'), options);
		}

	},

	resizeHighestDiv = function () {
		var $context = $('#solution'),
			$boxes = $('.box-stack li', $context);

		if (($context.length == 0) || $context.size() == 0) return;

		if($boxes.length) {
	        
	        //Get value of highest element
	        var maxHeight = Math.max.apply(Math, $.map($boxes,
	            function(element, index) {

	                var totalHeight = 0;

	                $(element).children().each(function() {
	                	totalHeight += $(this).outerHeight(); 
	                });
	                
	                return totalHeight;

	            }
	        ));

	        $boxes.height(maxHeight);
	    }  

	}

	// public functions
	return {

		init: function () {
			viewportWidth();
			//resizeHighestDiv();
			$(window).on('resize', function () { 
				viewportWidth();
				//resizeHighestDiv();
			});
			galleryHover();
			scrollLink();
			lightBox();
		}
	};

}());
